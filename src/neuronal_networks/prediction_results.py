import numpy as np


def create_prediction_result(dataset, model):
    # calc dataset size
    dataset_size = 0
    for (images, labels) in dataset.take(len(dataset)):
        dataset_size += len(images)

    class_names = dataset.class_names
    prediction_results = dict((label, dict((label, 0) for label in class_names)) for label in class_names)
    counter = 0

    for (images, labels) in dataset.take(len(dataset)):
        for i in range(len(images)):
            counter += 1
            print('Predicting image ' + str(counter) + ' of ' + str(dataset_size))

            image_class_index = np.argmax(labels[i])
            image_class = class_names[image_class_index]

            image = np.array([images[i]])
            prediction = model.predict(image)

            predicted_class_index = prediction.argmax(axis=1)[0]
            predicted_class = class_names[predicted_class_index]

            # fill result data structure
            recorded_image_class_predictions = prediction_results.get(image_class)
            recorded_image_class_predictions[predicted_class] += 1

    return prediction_results
