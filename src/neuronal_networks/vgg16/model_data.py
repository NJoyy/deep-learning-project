from keras.models import load_model

path = '../../../models/vgg16-model.h5'


def load_vgg16_model():
    return load_model(path)


def save_vgg16_model(model=None):
    model.save(path)
