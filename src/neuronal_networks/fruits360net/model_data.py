from keras.models import load_model

path = '../../../models/fruits360-model.h5'


def load_fruits360_model():
    return load_model(path)


def save_fruits360_model(model=None):
    model.save(path)
