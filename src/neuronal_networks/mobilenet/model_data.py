from keras.models import load_model

path = '../../../models/mobilenet-model.h5'


def load_mobilenet_model():
    return load_model(path)


def save_mobilenet_model(model=None):
    model.save(path)
