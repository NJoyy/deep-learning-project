import keras

train_dir = '../../../datasets/train/'
test_dir = '../../../datasets/test/'

color_mode = 'rgb'

batch_size = 32
validation_split = 0.5
labels = 'inferred'
label_mode = 'categorical'
seed = 12321  # random number


def get_train_dataset(image_height=100, image_width=100):
    return keras.preprocessing.image_dataset_from_directory(
        train_dir,
        image_size=(image_height, image_width),
        color_mode=color_mode,
        batch_size=batch_size,
        labels=labels,
        label_mode=label_mode,
        seed=seed
    )


def get_validation_dataset(image_height=100, image_width=100):
    return keras.preprocessing.image_dataset_from_directory(
        test_dir,
        image_size=(image_height, image_width),
        color_mode=color_mode,
        batch_size=batch_size,
        labels=labels,
        label_mode=label_mode,
        validation_split=validation_split,
        subset='validation',
        shuffle=True,
        seed=seed
    )


def get_test_dataset(image_height=100, image_width=100):
    return keras.preprocessing.image_dataset_from_directory(
        test_dir,
        image_size=(image_height, image_width),
        batch_size=batch_size,
        labels=labels,
        color_mode=color_mode,
        label_mode=label_mode,
        validation_split=validation_split,
        subset='training',
        shuffle=True,
        seed=seed
    )
