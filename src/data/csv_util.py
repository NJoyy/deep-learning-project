import csv


def create_csv(prediction_results, class_names, path):
    with open(path, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        headers_row = ['image class / predicted class']
        for class_name in class_names:
            headers_row.append(class_name)

        writer.writerow(headers_row)

        for (image_class, image_class_predictions) in prediction_results.items():
            row = [image_class]
            for val in image_class_predictions.values():
                row.append(val)
            writer.writerow(row)