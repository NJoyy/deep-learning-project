# Deep Learning Projekt - HTWK Leipzig - Modul Objekt- und Gestenerkennung - Wintersemester 2020/2021
***von Stephanie Erler, Fabian Mittmann***

In Zuge dieses Projektes wurden drei verschiedene neuronale Netze für die Erkennung von Früchten auf Bildern erstellt und trainiert.
Zum Trainieren wurde der Datensatz [Fruit 360 Data](https://www.kaggle.com/moltean/fruits) verwendet.



## Systemvoraussetzungen
* Python 3 (https://www.python.org/downloads/)
* pip (https://pypi.org/project/pip/)
* Jupyter Notebook (https://jupyter.org/install)



## Starten der Software

1. Zum Ausprobieren zunächst das Repository auschecken:
   
    `git clone https://gitlab.com/NJoyy/deep-learning-project.git`


2. Mittels der Python-Paketverwaltung *pip*  können im Projektverzeichnis alle benötigten Abhängigkeiten geladen werden:

    `pip install -r requirements.txt`


3. Unter `src/neuronal_networks/` stehen verschiedene Jupyter Notebooks zum Training und Erstellen von Vorhersagen für die jeweiligen Netze zur Verfügung:
    * `train.ipynb` zum Trainieren und Abspeichern des trainierten Modells
    * `predict_image.ipynb` zum Erstellen von Vorhersagen für einzelne Bilder auf dem trainierten Modell
    * `predict_test_data.ipynb` zum Erstellen von Vorhersagen für den gesamten Trainingsdatensatz auf dem trainierten Modell

